# coding=utf-8

from torform.field import Field

'''
自己实现一个类似于django中的forms.py的简单的form.py
用法示例：
   	class TestForm( Form ):
		name = StringField( required=True, error_msg_required='xxx' )
		age = IntegerField()
		email = EmailField()

	test_form = TestForm( self.request.aguments, self )
	if test_form.is_valid():
		print test_form.get_cleaned_data()
	else:
		print test_form.error
'''

'''
表基类：
	需要注意的一个地方是：request中的参数名字必须和Form中的一样。
'''
class BaseForm( object ):
	def __init__( self , request, handler=None ):
		self.class_attr = self.__class__.__dict__
		#得到检验过的数据
		self._cleaned_data = {}
		#请求数据
		self._request = request
		#静态变量列表
		self._static_attrs_list = [ attr for attr in self.class_attr if isinstance( self.class_attr[ attr ], Field ) ]
		#那个静态变量出错了
		#格式为{'age': [  ] , }
		self.error = {}
		#对于每一个静态变量
		for field in self._static_attrs_list:
			#先清理自身，不然会有以前处理过的痕迹
			self.class_attr[field]._clean_self()
			#如果request里边没有该数据的话，传值为空
			value = None
			#如果有的的话，将该值穿进去
			if field in self._request:
				value = self._request[ field ][ 0 ]
				if handler is not None:
					value = handler.get_argument( field, None )
					
			self.class_attr[field].initialize( value )
			#如果该变量出错了，将出错信息收集起来
			if self.class_attr[field].error:
				self.error[ field ] = self.class_attr[ field ].error
			#如果没出错，将其添加到cleand_data
			else:
				self._cleaned_data[ field ] = self.class_attr[field].cleaned_data
	#是否valid
	def is_valid( self ):
		return False if self.error else True

	#得到干净数据
	def get_cleaned_data( self ):
		return self._cleaned_data 
