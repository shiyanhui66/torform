#coding=utf-8
import re
import simplejson

__all__ = [ 'IntegerField', 'FloatField', 'BoolenField', 'StringField', 'JsonField', 'PasswordField', 'EmailField', 'ObjectIdField', 'ListField', 'IDField' ]

class Field( object ):
	def __init__( self , required=True, candidate=None, error_msg_required = 'required!', error_msg_format= 'format error!', error_candidate='error candidate' ):
		#表示Form中该变量是否能够为空值
		self.required = required
		# 可能的值
		self.candidate = candidate
		#required'表示self.required为True时，赋值却为空
		#format'表示该数据格式错误
		#里边的值代表错误类型
		self.error = [ ]
		#表示最终的数据
		self.cleaned_data = None
		#required为True，但是去没提供时的提示信息
		self.error_msg_required = error_msg_required
		#当格式错误时
		self.error_msg_format = error_msg_format
		self.error_candidate = error_candidate

	#得到该Field处理过后的数据
	def _clean_data( self , value ):
		self.cleaned_data = None
		if not self.error:
			self.cleaned_data = value

	#由于是静态变量，经过不断地处理，其错误结果会累积，因此需要一次处理完之后及时的清理
	def _clean_self( self ):
		self.error = []
		self.cleaned_data = None

	def initialize( self, value=None ):
		if self.candidate is not None and not isinstance( self.candidate, list ):
			raise Exception( 'candidate should be list' )
		if value is not None and self.candidate is not None and value not in self.candidate:
			self.error.append( self.error_candidate )

'''整数类型'''
class IntegerField( Field ):
	def __init__( self , required=True, candidate=None, min_value=None, max_value=None, error_msg_required = 'required!', error_msg_format= 'format error!', error_candidate='error candidate', 
		error_msg_min_value='value is less than min!', error_msg_max_value='value is larger than max' ):
		super( IntegerField, self ).__init__( required, candidate, error_msg_required, error_msg_format, error_candidate )
		#最大长度
		self.max_value = max_value
		#最短长度
		self.min_value = min_value
		#超过最大长度是的错误信息
		self.error_msg_max_value = error_msg_max_value
		#小过最小长度的错误信息
		self.error_msg_min_value = error_msg_min_value

	def initialize( self , value=None ):
		super( IntegerField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				self.cleaned_data = int( value )
			except Exception, e:
				self.error.append( self.error_msg_format )

			if self.min_value is not None and self.min_value > self.cleaned_data:
				self.error.append( self.error_msg_min_value )

			if self.max_value is not None and self.max_value < self.cleaned_data:
				self.error.append( self.error_msg_max_value )

		self._clean_data( self.cleaned_data )

'''浮点数类型'''
class FloatField( Field ):
	def __init__( self , required=True, candidate=None, min_value=None, max_value=None, error_msg_required = 'required!', error_msg_format= 'format error!', error_candidate='error candidate', 
		error_msg_min_value='value is less than min!', error_msg_max_value='value is larger than max' ):
		super( FloatField, self ).__init__( required, candidate, error_msg_required, error_msg_format, error_candidate )
		#最大长度
		self.max_value = max_value
		#最短长度
		self.min_value = min_value
		#超过最大数值时的错误信息
		self.error_msg_max_value = error_msg_max_value
		#小过最小数值的错误信息
		self.error_msg_min_value = error_msg_min_value

	def initialize( self , value=None ):
		super( FloatField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				self.cleaned_data = float( value )
				print self.cleaned_data
			except Exception, e:
				self.error.append( self.error_msg_format )

			if self.min_value is not None and self.min_value > self.cleaned_data:
				self.error.append( self.error_msg_min_value )

			if self.max_value is not None and self.max_value < self.cleaned_data:
				self.error.append( self.error_msg_max_value )
				
		self._clean_data( self.cleaned_data )

'''bool'''
class BoolenField( Field ):
	def initialize( self , value=None ):
		super( BoolenField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				self.cleaned_data = bool( value )
			except Exception, e:
				self.error.append( self.error_msg_format )
		self._clean_data( self.cleaned_data )

'''字符串类型'''
class StringField( Field ):
	def __init__( self , required=True, candidate=None, max_length=0 , min_length=0 , regex=None , error_msg_required = 'required!' , error_msg_format = 'format error!',\
	  error_msg_max_length='太长了' , error_msg_min_length='太短了', error_candidate='error candidate'):
		super( StringField, self ).__init__( required, candidate, error_msg_required , error_msg_format, error_candidate )
		#最大长度
		self.max_length = max_length
		#最短长度
		self.min_length = min_length
		#正则表达式
		self.regex = regex
		#超过最大长度是的错误信息
		self.error_msg_max_length = error_msg_max_length
		#小过最小长度的错误信息
		self.error_msg_min_length = error_msg_min_length

	def initialize( self , value=None ):
		super( StringField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			#注意：此处默认所传进来的max_length和min_length都是大于0的
			if self.max_length!=0 and len( value ) > self.max_length:
				self.error.append( self.error_msg_max_length )
			if self.min_length!=0 and len( value ) < self.min_length:
				self.error.append( self.error_msg_min_length )
			# 匹配正则表达式
			if self.regex:
				try:
					match = re.match( self.regex , value )
					if not match:
						self.error.append( self.error_msg_format ) 
				except Exception, e:
					self.error.append( self.error_msg_format ) 
		self._clean_data( value )

'''Json类型'''
class JsonField( Field ):
	def initialize( self , value=None ):
		super( JsonField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				self.cleaned_data = simplejson.loads( value )
			except Exception, e:
				self.error.append( self.error_msg_format )
		self._clean_data( self.cleaned_data )

'''Email类型'''
class EmailField( Field ):
	def initialize( self , value=None ):
		super( EmailField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				#email正则表达式
				email_re = re.compile(
				    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
				    # quoted-string, see also http://tools.ietf.org/html/rfc2822#section-3.2.5
				    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"'
				    r')@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$)'  # domain
				    r'|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$', re.IGNORECASE )
				match = re.match( email_re , value )
				if not match:
					self.error.append( self.error_msg_format )
			except Exception, e:
				self.error.append( self.error_msg_format )
		self._clean_data( value )

'''密码类型'''
class PasswordField( Field ):
	def initialize( self, value=None ):
		super( PasswordField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				# 6-20位数字、字母、特殊字符
				password_re = re.compile( '''[-\da-zA-Z`=\\\[\];',./~!@#$%^&*()_+|{}:"<>?]{6,20}''' )
				match = re.match( password_re, value )
				if not match:
					self.error.append( self.error_msg_format )
			except Exception:
				self.error.append( self.error_msg_format )
		self._clean_data( value )

class IDField( Field ):
	chmap = { '0':0,'1':1,'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'x':10,'X':10 }  
	def ch_to_num( self, ch ):  
	    return IDField.chmap[ch]      

	def verify_string( self, s ):  
	    char_list = list(s)  
	    num_list = [ self.ch_to_num(ch) for ch in char_list]  
	    return self.verify_list(num_list)  

	def verify_list( self, l):  
	    sum = 0  
	    for ii,n in enumerate(l):  
	        i = 18-ii  
	        weight = 2**(i-1) % 11  
	        sum = (sum + n*weight) % 11  
	    return sum==1  

	def initialize( self, value=None ):
		super( IDField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				# 6-20位数字、字母、特殊字符
				ID_re = re.compile( '^[1-9][0-9]{16}[x0-9]$', re.IGNORECASE )
				match = re.match( ID_re, value )
				if not match:
					self.error.append( self.error_msg_format )
				elif not self.verify_string( value ):
					self.error.append( self.error_msg_format )
			except:
				self.error.append( self.error_msg_format )
		self._clean_data( value )

class ObjectIdField( Field ):
	def initialize( self, value=None ):
		super( ObjectIdField, self ).initialize( value )
		if not value:
			if self.required:
				self.error.append( self.error_msg_required )
		else:
			try:
				# 6-20位数字、字母、特殊字符
				objectid_re = re.compile( '[a-f0-9]{24}' )
				match = re.match( objectid_re, value )
				if not match:
					self.error.append( self.error_msg_format )
			except Exception:
				self.error.append( self.error_msg_format )
		self._clean_data( value )

class ListField( Field ):
	def initialize( self, value=None ):
		super( ListField, self ).initialize( value )
		if value is None and self.required:
			self.error.append( self.error_msg_required )
		else:
			if not isinstance( value, list ):
				self.error.append( self.error_msg_format )
		self._clean_data( value )