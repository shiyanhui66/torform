Dependencies
-----------

* simplejson
* re

Install
--------

    python setup.py install 

Example
-------
	from torform.field import *
	from torform.form import BaseForm

	class ExampleForm( BaseFrom ):
		a = IntegerField( required=True, error_msg_required='' )
		b = StringField( required=False )

	class ExampleHandler( RequestHandler ):
		def post( self ):
			example_form = ExampleForm( self.request.arguments, self )
			if example_form.is_valid():
				cleaned_data = example_form.get_cleaned_data()
			else:
				print example_form.error
			...
